webpackJsonp([22],{

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationListPageModule", function() { return NotificationListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification_list__ = __webpack_require__(491);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificationListPageModule = /** @class */ (function () {
    function NotificationListPageModule() {
    }
    NotificationListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notification_list__["a" /* NotificationListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__notification_list__["a" /* NotificationListPage */]),
            ],
        })
    ], NotificationListPageModule);
    return NotificationListPageModule;
}());

//# sourceMappingURL=notification-list.module.js.map

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_member__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationListPage = /** @class */ (function () {
    function NotificationListPage(navCtrl, navParams, loadingCtrl, alertCtrl, memberProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.memberProvider = memberProvider;
        this.isLoading = true;
        this.memberId = 0;
        this.dataList = navParams.get('data');
        this.memberId = navParams.get('memberId');
        this.isLoading = false;
    }
    NotificationListPage.prototype.ionViewWillEnter = function () {
    };
    NotificationListPage.prototype.openDetail = function (blogItem) {
        var _this = this;
        var alert = this.alertCtrl.create({ title: blogItem.Date, message: blogItem.Message, buttons: ["OK"] });
        alert.onDidDismiss(function () {
            var model = {
                MemberId: _this.memberId,
                MessageId: blogItem.Id,
                Type: "Read",
                Lang: __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage
            };
            _this.memberProvider.notificationSave(model).subscribe(function (result) {
                _this.navCtrl.pop();
            });
        });
        alert.present();
    };
    NotificationListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notification-list',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\notification-list\notification-list.html"*/'<ion-header mode="ios">\n  <ion-navbar transparent mode="ios">\n    <ion-title mode="ios">Bildirimler </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-list *ngIf="!isLoading">\n    <ion-item *ngFor="let item of dataList; let orderNo = index;" (click)="openDetail(item)">\n      <span class="number {{item.IsRead ? \'\':\'unread\'}}"></span>\n      <h2>{{item.Date}}</h2>\n      <p>{{item.Message }}</p>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\notification-list\notification-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_member__["a" /* MemberProvider */]])
    ], NotificationListPage);
    return NotificationListPage;
}());

//# sourceMappingURL=notification-list.js.map

/***/ })

});
//# sourceMappingURL=22.js.map