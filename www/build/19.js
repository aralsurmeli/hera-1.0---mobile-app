webpackJsonp([19],{

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SaglikKayitlarimPageModule", function() { return SaglikKayitlarimPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saglik_kayitlarim__ = __webpack_require__(510);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SaglikKayitlarimPageModule = /** @class */ (function () {
    function SaglikKayitlarimPageModule() {
    }
    SaglikKayitlarimPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saglik_kayitlarim__["a" /* SaglikKayitlarimPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__saglik_kayitlarim__["a" /* SaglikKayitlarimPage */]),
            ],
        })
    ], SaglikKayitlarimPageModule);
    return SaglikKayitlarimPageModule;
}());

//# sourceMappingURL=saglik-kayitlarim.module.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SaglikKayitlarimPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_member__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SaglikKayitlarimPage = /** @class */ (function () {
    function SaglikKayitlarimPage(navCtrl, loadingCtrl, commonProvider, memberProvider) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.commonProvider = commonProvider;
        this.memberProvider = memberProvider;
        this.isLoading = true;
        this.translation = {};
    }
    SaglikKayitlarimPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var translationSource = __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'My Health Records'; })[0];
        this.translation = translationSource.Translation;
        this.commonProvider.getMember().then(function (result) {
            if (result && result.MemberId && result.MemberId > 0) {
                _this.memberInfo = result;
                var loading_1 = _this.loadingCtrl.create({ content: __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].loadingMsg });
                loading_1.present();
                var model = { MemberId: result.MemberId, Lang: __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].currentLanguage };
                _this.memberProvider.healthRecordList(model).subscribe(function (result) {
                    loading_1.dismiss();
                    _this.isLoading = false;
                    _this.dataList = result.Result;
                });
            }
            else {
                _this.navCtrl.setRoot("LoginPage");
            }
        });
    };
    SaglikKayitlarimPage.prototype.call = function () {
        this.navCtrl.push("SpRaporPage", { memberId: this.memberInfo.MemberId });
    };
    SaglikKayitlarimPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-saglik-kayitlarim',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\saglik-kayitlarim\saglik-kayitlarim.html"*/'<ion-header mode="ios">\n  <ion-navbar transparent mode="ios">\n    <ion-title mode="ios"> {{translation.lblHeader}} </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="!isLoading">\n      <ion-row *ngFor="let item of dataList">\n          <ion-col>\n            <ion-card>\n              <img [src]="item.Photo" />\n              <ion-card-content>\n                <ion-card-title>\n                  {{item.Name}}\n                </ion-card-title>\n                <p>\n                  <ion-icon name="md-calendar"></ion-icon>\n                  {{item.Date}}\n                </p>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n  </div>\n  \n\n</ion-content>\n<ion-footer padding>\n  <button ion-button color="secondary" block (click)="call()">\n    {{translation.btnReportUpload}}\n  </button>\n</ion-footer>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\saglik-kayitlarim\saglik-kayitlarim.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_member__["a" /* MemberProvider */]])
    ], SaglikKayitlarimPage);
    return SaglikKayitlarimPage;
}());

//# sourceMappingURL=saglik-kayitlarim.js.map

/***/ })

});
//# sourceMappingURL=19.js.map