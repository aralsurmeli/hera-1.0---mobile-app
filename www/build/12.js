webpackJsonp([12],{

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDataAggPageModule", function() { return UserDataAggPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_data_agg__ = __webpack_require__(508);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UserDataAggPageModule = /** @class */ (function () {
    function UserDataAggPageModule() {
    }
    UserDataAggPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__user_data_agg__["a" /* UserDataAggPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__user_data_agg__["a" /* UserDataAggPage */]),
            ],
        })
    ], UserDataAggPageModule);
    return UserDataAggPageModule;
}());

//# sourceMappingURL=user-data-agg.module.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDataAggPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserDataAggPage = /** @class */ (function () {
    function UserDataAggPage(navCtrl, navParams, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.superTabsCtrl = superTabsCtrl;
        this.translation = {};
        this.lang = "tr";
        this.onlyShow = false;
        this.lang = __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */].currentLanguage;
        this.onlyShow = this.navParams.get('onlyShow') || false;
    }
    UserDataAggPage.prototype.ionViewWillEnter = function () {
        var translationSource = __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'User Agreement'; })[0];
        this.translation = translationSource.Translation;
    };
    UserDataAggPage.prototype.next = function () {
        this.superTabsCtrl.slideTo(2);
    };
    UserDataAggPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-user-data-agg',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\user-data-agg\user-data-agg.html"*/'<ion-header mode="ios">\n  <ion-navbar transparent mode="ios">\n    <ion-buttons left *ngIf="onlyShow">\n      <button navPop ion-button icon-only>\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title mode="ios">{{translation.lblPersonalInfo}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf="lang==\'tr\'">\n    <strong>KİŞİSEL VERİLERİN KORUNMASINA İLİŞKİN AYDINLATMA VE AÇIK RIZA METNİ </strong><br>\n    <p>\n      Kişisel verilerinizin gizliliği ve güvenliğine önem veriyoruz. 6698 sayılı Kişisel Verilerin Korunması Kanunu\n      (“KVKK”) kapsamında MEDAK/Medikal Arama Kurtarma Derneği olarak Veri Sorumlusu sıfatıyla sizleri aydınlatmak\n      istiyoruz. Kişisel verilerinizin, 6698 sayılı Kişisel Verilerin Korunması Kanunu uyarınca, Veri Sorumlusu\n      sıfatıyla MEDAK tarafından hangi kapsamda işlenebileceği aşağıda açıklanmıştır.\n    </p><br>\n    <strong>I. Kişisel Verilerinizi Toplamamızın Yöntemi ve Hukuki Sebebi Nedir?</strong><br>\n    <p>\n      Kişisel verileriniz, uygulamaya ilişkin faaliyetlerimizi yürütmek adına MEDAK tarafından farklı kanallarla ve\n      mevzuata ve dernek politikalarına uyumun sağlanması hukuki sebeplerine dayanılarak toplanmaktadır. KVKK gereğince,\n      HERA uygulaması vasıtasıyla tarafımızla paylaşmış olduğunuz kişisel verileriniz, Veri Sorumlusu sıfatıyla\n      tarafımızca, KVKK’da açıklandığı çerçevede; elde edilebilecek, kaydedilebilecek, depolanabilecek, muhafaza\n      edilebilecek, hizmetlerimizin devamının sağlanabilmesi amacıyla güncellenebilecek, değiştirilmesi sağlanabilecek,\n      yeniden düzenlenebilecek, mevzuatın izin verdiği durumlarda ve ölçüde üçüncü kişilere açıklanabilecek ve\n      işlenebilecektir. Kişisel verileriniz, yalnızca KVKK tarafından öngörülen temel ilkelere uygun olarak ve KVKK’nın\n      5. ve 6. maddelerinde belirtilen kişisel veri işleme şartları ve amaçları kapsamında işbu Aydınlatma Metni’nde\n      belirtilen amaçlarla işlenebilmekte ve aktarılabilmektedir. Genel olarak, kişisel verileriniz, derneğimiz\n      tarafından yürütülmekte olan faaliyetlere bağlı olarak değişkenlik göstermekle birlikte, mevzuat, sözleşme ve\n      teknoloji gerekliliklerine uygun olarak dernek birimlerinin yürüttüğü operasyon faaliyetlerini yerine getirebilmek\n      için toplanabilmektedir.\n    </p><br>\n    <strong> II. Kişisel Verilerinizi Hangi Amaçla İşliyoruz? </strong><br>\n    <p>\n      Toplanan kişisel verileriniz, KVKK tarafından öngörülen temel ilkelere uygun olarak ve KVKK’nın 5. ve 6.\n      maddelerinde belirtilen kişisel veri işleme şartları ve amaçları dâhilinde, MEDAK tarafından aşağıda yer alan\n      amaçlarla işlenebilmektedir:\n    </p><br>\n    <ul>\n      <li>MEDAK’ın sahip olduğu yasal ve idari yükümlülüklerin mevzuata uygun olarak yerine getirmesi,</li>\n      <li>Dernek içi koordinasyon, iş birliği ve verimliliğin sağlanması ve sürdürülmesi,</li>\n      <li>Derneğin taraf olduğu sözleşmelere ve kanuna aykırılıkların soruşturulması, tespiti, önlenmesi ve ilgili idari\n        veya adli makamlara bildirilmesi,</li>\n      <li>Mevcut veya ileride meydana gelebilecek hukuki uyuşmazlıkların çözülmesi,</li>\n      <li>Talep ve soruların cevaplandırılması,</li>\n      <li>Derneğin ticari itibarının ve oluşturduğu güvenin korunması. </li>\n    </ul>\n    <strong>III. Kişisel Verilerinizi Kimlere ve Hangi Amaçla Aktarıyoruz?</strong><br>\n    <p> Toplanan kişisel verileriniz; KVKK tarafından öngörülen temel ilkelere uygun olarak ve KVKK’nın 8. ve 9.\n      maddelerinde belirtilen kişisel veri işleme şartları ve amaçları dâhilinde, MEDAK tarafından yukarıda belirtilen\n      amaçlarla; iş ortaklarımız, iş bağlantılarımız, ifa yardımcılarımız ve alt yüklenicilerimiz ile derneğimizin\n      faaliyetleri doğrultusunda ya da düzenleyici denetleyici kurumlar ve resmi mercilerle ilgili mevzuatın öngördüğü\n      durumlarda yurt içine veya yurt dışına aktarılabilecektir. Faaliyetlerimizi yürütebilmek amacıyla (örneğin bilgi\n      sistemleri yönetimi ve depolanması, veri analizi vb. konularda destek almak) kişisel verilerinizi, 3. kişi hizmet\n      sağlayıcılarına açıklayabiliriz. Bu aktarımlarda paylaşılan kişisel verilerin verilecek hizmetle sınırlı olmasına\n      özen gösterilir ve aktarılan kişisel verilerinizin mevzuatın gerektirdiği şekilde korunması için tüm tedbirler\n      alınır. </p><br>\n    <p>\n      Yukarıdaki bilgilendirme dokümanın tamamını okuduğunuzu, anladığınızı ve KVKK kapsamında kişisel verilerinizin;\n      <br>Yukarıda belirtilen yöntemler ile toplanmasına ve yine yukarıda belirtilen amaçlar dâhilinde işlenmesine,\n      <br>Yukarıda belirtilen yurtiçi ve yurtdışındaki üçüncü kişilere aktarılmasına, yine yukarıda belirtilen amaçlar\n      dâhilinde yurtdışına aktarılmasına/paylaşılmasına,\n      açık rıza verdiğinizi işbu metni imzalamak suretiyle açıkça beyan etmektesiniz.\n    </p><br>\n  </div>\n  <div *ngIf="lang==\'ar\'">\n    نص الإعلام المستنير والموافقة الواضحة\n    حول حماية البيانات الشخصية (KVKK)<br>\n\n    نحن نهتم بخصوصية وأمان بياناتكم الشخصية. ضمن القانون رقم 6698 بشأن حماية البيانات الشخصية (KVKK.) بصفتنا MEDAK/\n    جمعية البحث والإنقاذ الطبي، نود أن ننوركم كموظفين مسؤولين البيانات. موضح أدناه ماهية المجالات التي يمكن معالجة\n    بياناتك الشخصية فيها من قبل موظف مسؤول البيانات في MEDAK وفقًا للقانون رقم 6698 بشأن حماية البيانات الشخصية.<br>\n    <br>I. ما هي الطريقة والسبب القانوني لجمع بياناتكم الشخصية؟\n    يتم جمع بياناتكم الشخصية على أساس أسباب قانونية من قبل MEDAK لضمان الامتثال للقنوات والتشريعات المختلفة وسياسات\n    الجمعية من أجل تنفيذ الأنشطة المتعلقة بالتطبيق وفقًا لـ KVKK، فإن بياناتكم الشخصية التي قمتم بمشاركتها معنا من خلال\n    تطبيق HERA، بصفتنا كمسؤولين البيانات وكما هو موضح في إطار KVKK ؛ سنكون قادرين على الحصول عليها, تسجيلها, حفظها,\n    وتخزينها بالإضافة الى إمكانية تحديثها, تغييرها, تعديلها, إعادة تنظيمها والإفصاح عنها الى أطراف ثالثة إذا كان ذلك\n    مسموحًا به في التشريعات وضمن المدى المعمول به بهدف ضمان استمرارية خدماتنا. لا يمكن معالجة بياناتكم الشخصية ونقلها\n    إلا للأغراض المنصوص عليها في نص الموافقة المستنيرة هذا وفقًا للمبادئ الأساسية المنصوص عليها من قبل KVKK ووفقًا لشروط\n    وأغراض معالجة البيانات الشخصية المنصوص عليها في المادتين 5 و 6 من KVKK. بشكل عام وبالإضافة الى اختلاف بياناتكم\n    الشخصية اعتمادًا على الأنشطة التي تقوم بها جمعيتنا ، فإنه يقد يتم جمعها من أجل أداء فعاليات وحدات الجمعيات وفقًا\n    للتشريعات والعقود ومتطلبات التكنولوجيا.\n    <br>II. لأي غرض نقوم بمعالجة بياناتكم الشخصية؟\n    يجوز لـ MEDAK معالجة بياناتكم الشخصية للأغراض المذكورة أدناه وفقًا للمبادئ الأساسية المنصوص عليها في KVKK ووفقًا\n    لشروط وأغراض معالجة البيانات الشخصية المنصوص عليها في المادتين 5 و 6 من KVKK:\n    <br>• للوفاء بالالتزامات القانونية والإدارية لـ MEDAK بشكل مناسب للتشريعات،\n    <br>• للضمان والحفاظ على التنسيق داخل الجمعية بالإضافة الى التعاون والكفاءة،\n    <br>• التحقيق والكشف والوقاية والإبلاغ إلى السلطات الإدارية أو القضائية ذات الصلة بالاتفاقيات والأفعال غير القانونية\n    التي تكون الجمعية طرفًا فيها،\n    <br>• لحل النزاعات القانونية الحالية أو التي قد تطرأ في المستقبل،\n    <br>• الرد على الأسئلة والطلبات،\n    <br>• لحماية السمعة التجارية والثقة بالجمعية.\n    <br>III. الى من ننقل بياناتكم الشخصية ولأية أغراض؟\n    البيانات الشخصية الخاصة بكم التي تم جمعها وفقًا للمبادئ الأساسية المنصوص عليها في KVKK ووفقًا لشروط معالجة البيانات\n    الشخصية والأغراض المنصوص عليها في المادتين 8 و 9 من KVKK، فإنه يمكن نقلها الى داخل أو خارج البلاد في الحالات المنصوص\n    عليها في التشريعات المعمول بها ووكالات الرقابة التنظيمية وفقا لأنشطة جمعيتنا ومشاركتها مع شركائنا التجاريين, جهات\n    الاتصال التجارية, مساعدين الأداء والمقاولين من الباطن للأغراض المذكورة أعلاه من قبل MEDAK. يجوز لنا الكشف عن\n    بياناتكم الشخصية لمقدمي الخدمات من طرف ثالث بهدف تنفيذ أنشطتنا (مثل إدارة نظام المعلومات وتخزينه، تحليل البيانات،\n    وما إلى ذلك). في عمليات النقل هذه، يتم الحرص على أن تقتصر البيانات الشخصية المشتركة على الخدمة التي يتم تقديمها، كما\n    يتم اتخاذ جميع التدابير الضرورية من أجل حماية بياناتكم الشخصية كما هو مطلوب بموجب التشريعات.\n\n    <br>\n    قرائتكم وفهمكم لكامل وثيقة الموافقة المستنيرة, ومن الناحية المتعلقة ببياناتكم الشخصية ضمن نطاق KVKK؛\n    <br>• فإنكم تعربون بوضوح عن موافقتكم الصريحة بواسطة التوقيع على هذا النص عن:\n    <br>• أنه يجوز جمع بياناتكم الشخصية بالأساليب المذكورة أعلاه وإمكانية معالجتها وفقا للأغراض المذكورة أعلاه أيضا,\n    مشاركتها مع الأطراف الثالثة المذكورة أعلاه داخل وخارج البلاد بالإضافة الى مشاركتها/ نقلها الى خارج البلاد وفقا\n    للأغراض المذكورة أعلاه أيضا.\n\n  </div>\n  <div *ngIf="lang==\'en\'">\n    <strong>CLARIFICATION AND EXPRESS CONSENT TEXT\n      FOR THE PROTECTION OF PERSONAL DATA</strong>\n    <br>\n    We care about the privacy and security of your personal data. Under the Law on the Protection of Personal Data No.\n    6698 ("LPPD"), we would like to enlighten you as MEDAK / Medical Search and Rescue Association in the capacity of\n    Data Supervisor. In accordance with the Law on the Protection of Personal Data No. 6698, the extent to which MEDAK\n    may process your personal data in the capacity of Data Supervisor is explained below.<br>\n    <strong> I. What are the Method and the Legal Reason of Collecting Your Personal Data by Us?</strong><br>\n    MEDAK collects your personal data to carry out our activities on the basis of legal reasons to ensure compliance\n    with different channels, legislation, and association policies. As per LPPD, the personal data you have shared with\n    us via the HERA application may be obtained, recorded, stored, maintained, updated for the continuation of our\n    services, modified, reorganized, disclosed to third parties in the cases and to the extent permitted by the\n    legislation and processed by us in the capacity of Data Supervisor within the framework defined in LPPD. Your\n    personal data may only be processed and transferred for the purposes set out in this Clarification Text in\n    accordance with the basic principles stipulated by the LPPD and within the scope of the personal data processing\n    conditions and purposes set forth in Articles 5 and 6 of the LPPD. Although it may vary depending on the activities\n    carried out by our Association, generally, your personal data may be collected to perform the operational activities\n    carried out by the Association units in accordance with the legislation, contract, and technological requirements.\n    <strong>II. For What Purpose Do We Process Your Personal Data?</strong><br>\n    MEDAK may only process your personal data for the purposes set out below in accordance with the basic principles\n    stipulated by the LPPD and within the scope of the personal data processing conditions and purposes set forth in\n    Articles 5 and 6 of the LPPD:\n    <ul>\n      <li>Fulfillment of the legal and administrative obligations of MEDAK in accordance with the legislation,</li>\n      <li>Ensuring and maintaining coordination, cooperation, and efficiency within the Association,</li>\n      <li>The investigation, detection, prevention and notification to the relevant administrative or judicial authorities\n          of the contradictions to the law in the contracts to which the Association is a party,</li>\n          <li>Elimination of legal disputes that may exist or may occur in the future,</li>\n          <li>Responding to questions and requests,</li>\n          <li>Protection of the commercial reputation and the developed trust of the Association.</li>\n    </ul>\n    <strong>III. To Whom and For What Purposes Do We Transfer Your Personal Data?</strong><br>\n    Your personal data collected may only be transferred by MEDAK for the purposes set out above in accordance with the\n    basic principles stipulated by the LPPD and within the scope of the personal data processing conditions and\n    objectives set forth in Articles 8 and 9 of the LPPD to our business partners, business contacts, performance\n    assistants and subcontractors and to home or abroad in accordance with the activities of our Association or in the\n    cases stipulated by the legislation on regulatory-supervisory authorities and governmental entities. We may disclose\n    your personal data to third party service providers to conduct our activities (for instance to get support on issues\n    such as management and storage of information systems, data analysis, etc.). These transfers ensure that your shared\n    personal data is limited to the service to be provided, and all necessary measures are taken to protect your\n    personal data as required by the legislation.\n    <br>\n    By signing this text, in accordance with LPPD, you hereby explicitly declare that you have read, understood and\n    given express consent to\n    <ul>\n      <li>the collection and processing of your personal data using the methods mentioned above,</li>\n      <li>the transfer of your personal data to the above-mentioned third parties home and abroad and the\n          transference/sharing of your personal data to the elsewhere within the scope of the purposes mentioned above,</li>\n    </ul>\n  </div>\n\n</ion-content>\n\n<ion-footer padding text-center *ngIf="!onlyShow">\n  <button ion-button (click)="next()">{{translation.btnAccept}}</button>\n</ion-footer>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\user-data-agg\user-data-agg.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__["a" /* SuperTabsController */]])
    ], UserDataAggPage);
    return UserDataAggPage;
}());

//# sourceMappingURL=user-data-agg.js.map

/***/ })

});
//# sourceMappingURL=12.js.map