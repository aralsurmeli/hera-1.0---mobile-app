webpackJsonp([15],{

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpHModalPageModule", function() { return SpHModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sp_h_modal__ = __webpack_require__(502);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SpHModalPageModule = /** @class */ (function () {
    function SpHModalPageModule() {
    }
    SpHModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sp_h_modal__["a" /* SpHModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sp_h_modal__["a" /* SpHModalPage */]),
            ],
        })
    ], SpHModalPageModule);
    return SpHModalPageModule;
}());

//# sourceMappingURL=sp-h-modal.module.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpHModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpHModalPage = /** @class */ (function () {
    function SpHModalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.week = 0;
        this.week = parseInt(navParams.get('week'));
        console.log("Modal => ", this.week);
    }
    SpHModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpHModalPage');
    };
    SpHModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sp-h-modal',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\sp-h-modal\sp-h-modal.html"*/'\n<ion-content padding>\n<ion-row>\n  <ion-col col-12>\n    <img src="assets/imgs/pregnant-success.svg" class="plogo">\n  </ion-col>\n</ion-row>\n<ion-row>\n  <ion-col text-center>\n    <h5>Tebrikler,<strong>{{week}} haftalık</strong> hamilesiniz.</h5>\n    <!-- <ion-note><ion-icon name="clock"></ion-icon> 20.05.2019</ion-note> -->\n  </ion-col>\n</ion-row>\n\n<ion-row>\n  <ion-col col-12>\n      <button mode="ios" navPop ion-button block outline color="secondary">Tamam</button>\n  </ion-col>\n</ion-row>\n\n</ion-content>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\sp-h-modal\sp-h-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SpHModalPage);
    return SpHModalPage;
}());

//# sourceMappingURL=sp-h-modal.js.map

/***/ })

});
//# sourceMappingURL=15.js.map