webpackJsonp([14],{

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpRaporPageModule", function() { return SpRaporPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sp_rapor__ = __webpack_require__(504);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SpRaporPageModule = /** @class */ (function () {
    function SpRaporPageModule() {
    }
    SpRaporPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sp_rapor__["a" /* SpRaporPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sp_rapor__["a" /* SpRaporPage */]),
            ],
        })
    ], SpRaporPageModule);
    return SpRaporPageModule;
}());

//# sourceMappingURL=sp-rapor.module.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpRaporPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_member__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SpRaporPage = /** @class */ (function () {
    function SpRaporPage(navCtrl, actionSheetCtrl, camera, loadingCtrl, alertCtrl, memberProvider, formBuilder, navParams) {
        this.navCtrl = navCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.memberProvider = memberProvider;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.memberId = 0;
        this.photoSelected = false;
        this.selectedPhoto = "";
        this.translation = {};
        this.memberId = navParams.get('memberId');
        this.frmRecord = this.formBuilder.group({
            Name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            Photo: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required]
        });
    }
    SpRaporPage.prototype.ionViewWillEnter = function () {
        var translationSource = __WEBPACK_IMPORTED_MODULE_5__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'My Health Records'; })[0];
        this.translation = translationSource.Translation;
    };
    SpRaporPage.prototype.call = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: this.translation.lblReport,
            buttons: [{
                    text: this.translation.lblTakePhoto,
                    handler: function () {
                        _this.takePhoto(1);
                    }
                }, {
                    text: this.translation.lblFromGallery,
                    handler: function () {
                        _this.takePhoto(0);
                    }
                }, {
                    text: this.translation.lblCancel,
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    SpRaporPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        var options = {
            quality: 75,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: sourceType,
            correctOrientation: true,
            saveToPhotoAlbum: false
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.selectedPhoto = 'data:image/jpeg;base64,' + imageData;
            _this.frmRecord.controls['Photo'].setValue(_this.selectedPhoto);
            _this.photoSelected = true;
        }, function (err) {
            // Handle error
        });
    };
    SpRaporPage.prototype.saveRecord = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: this.translation.lblLoading });
        loading.present();
        var model = {
            Id: 0,
            Date: '',
            MemberId: this.memberId,
            Name: this.frmRecord.value.Name,
            Photo: this.selectedPhoto
        };
        this.memberProvider.saveHealthRecord(model).subscribe(function (result) {
            loading.dismiss();
            //let alert = this.alertCtrl.create({title:'Hera',message:'Rapor yükleme işlemi tamamlandı',buttons:["OK"]});
            //alert.onDidDismiss(()=>{
            _this.navCtrl.pop();
            //});
            //alert.present();
        });
    };
    SpRaporPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sp-rapor',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\sp-rapor\sp-rapor.html"*/'<ion-header mode="ios">\n    <ion-navbar transparent mode="ios">\n      <ion-title mode="ios">{{translation.btnReportUpload}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <div class="logo" *ngIf="!photoSelected">\n    <img src="assets/imgs/camera-area.svg">\n  </div>\n  <h2 *ngIf="!photoSelected"> {{translation.lblDescription}}\n    <br>\n    <br>\n    <strong>{{translation.lblInfo}}</strong>\n  </h2>\n\n  <ion-row *ngIf="!photoSelected">\n    <ion-col>\n      <button ion-button color="secondary" class="callButton" block (click)="call()">\n          <ion-icon start name="camera"></ion-icon>\n          {{translation.btnReportUpload}}\n          <ion-icon end name="arrow-forward"></ion-icon>\n      </button>\n    </ion-col>\n  </ion-row>\n\n  <form [formGroup]="frmRecord" no-lines *ngIf="photoSelected">\n      <div class="logo">\n          <img [src]="selectedPhoto">\n      </div>\n      <ion-item>\n        <ion-label  mode="ios" color="white" floating>{{translation.lblFormDescription}}</ion-label>\n        <ion-textarea rows="3" placeholder="" type="text"  formControlName="Name" ></ion-textarea>\n      </ion-item>\n      <div class="error-message" *ngIf="!frmRecord.get(\'Name\').valid && (frmRecord.get(\'Name\').dirty || frmRecord.get(\'Name\').touched)"> \n          <div *ngIf="frmRecord.controls.Name.hasError(\'required\')"> {{translation.errFormDescription}} </div> \n      </div>      \n  </form>\n  <br>\n  <br>\n  <button *ngIf="photoSelected" ion-button block (click)="saveRecord()" [disabled]="!frmRecord.valid" color="secondary" class="callButton">\n      <ion-icon start name="camera"></ion-icon>\n      {{translation.btnReportUpload}}\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\sp-rapor\sp-rapor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_member__["a" /* MemberProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SpRaporPage);
    return SpRaporPage;
}());

//# sourceMappingURL=sp-rapor.js.map

/***/ })

});
//# sourceMappingURL=14.js.map