webpackJsonp([18],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SaglikProfilimPageModule", function() { return SaglikProfilimPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saglik_profilim__ = __webpack_require__(497);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SaglikProfilimPageModule = /** @class */ (function () {
    function SaglikProfilimPageModule() {
    }
    SaglikProfilimPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saglik_profilim__["a" /* SaglikProfilimPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__saglik_profilim__["a" /* SaglikProfilimPage */]),
            ],
        })
    ], SaglikProfilimPageModule);
    return SaglikProfilimPageModule;
}());

//# sourceMappingURL=saglik-profilim.module.js.map

/***/ }),

/***/ 497:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SaglikProfilimPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SaglikProfilimPage = /** @class */ (function () {
    function SaglikProfilimPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.translation = {};
    }
    SaglikProfilimPage.prototype.ionViewWillEnter = function () {
        var translationSource = __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'Health Profile'; })[0];
        this.translation = translationSource.Translation;
    };
    SaglikProfilimPage.prototype.openCategory = function (categoryId) {
        if (parseInt(categoryId) === 4) {
            this.navCtrl.push("SpCocukPage", { isRegister: false });
        }
        else {
            this.navCtrl.push('QuestionFormPage', { categoryId: categoryId });
        }
    };
    SaglikProfilimPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-saglik-profilim',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\saglik-profilim\saglik-profilim.html"*/'<ion-header mode="ios">\n  <ion-navbar transparent mode="ios">\n    <ion-title mode="ios">{{translation.lblTitle}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="logo">\n    <ion-icon name="md-person"></ion-icon>\n  </div>\n  <ion-list>\n    <ion-item (click)="openCategory(1)">\n      <img src="assets/imgs/heartbeat.svg">\n      <h2>{{translation.lblGeneralHealthInfo}}</h2>\n      <p>{{translation.lblGeneralHealthInfoDesc}}</p>\n    </ion-item>\n\n    <ion-item (click)="openCategory(2)">\n      <img src="assets/imgs/care.svg">\n      <h2>{{translation.lblFamilyInfo}}</h2>\n      <p>{{translation.lblFamilyInfoDesc}}</p>\n    </ion-item>\n\n    <ion-item (click)="openCategory(3)">\n      <img src="assets/imgs/medicine.svg">\n      <h2>{{translation.lblPersonalHealthInfo}}</h2>\n      <p>{{translation.lblPersonalHealthInfoDesc}}</p>\n    </ion-item>\n\n    <ion-item (click)="openCategory(4)">\n      <img src="assets/imgs/pediatry.svg">\n      <h2>{{translation.lblChildInfo}}</h2>\n      <p>{{translation.lblChildInfoDesc}}</p>\n    </ion-item>\n\n    <ion-item (click)="openCategory(5)">\n      <img src="assets/imgs/maternity.svg">\n      <h2>{{translation.lblPregnancy}}</h2>\n      <p>{{translation.lblPregnancyDesc}}</p>\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\saglik-profilim\saglik-profilim.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], SaglikProfilimPage);
    return SaglikProfilimPage;
}());

//# sourceMappingURL=saglik-profilim.js.map

/***/ })

});
//# sourceMappingURL=18.js.map