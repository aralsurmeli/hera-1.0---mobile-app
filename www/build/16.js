webpackJsonp([16],{

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpCocukPageModule", function() { return SpCocukPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sp_cocuk__ = __webpack_require__(500);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SpCocukPageModule = /** @class */ (function () {
    function SpCocukPageModule() {
    }
    SpCocukPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sp_cocuk__["a" /* SpCocukPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sp_cocuk__["a" /* SpCocukPage */]),
            ],
        })
    ], SpCocukPageModule);
    return SpCocukPageModule;
}());

//# sourceMappingURL=sp-cocuk.module.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpCocukPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_member__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_component__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_common__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_navigation_nav_params__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SpCocukPage = /** @class */ (function () {
    function SpCocukPage(navCtrl, navParams, memberProvider, commonProvider, loadingCtrl, alertCtrl, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.memberProvider = memberProvider;
        this.commonProvider = commonProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.superTabsCtrl = superTabsCtrl;
        this.isLoading = true;
        this.isRegister = true;
        this.translation = {};
        this.isRegister = this.navParams.get('isRegister') === undefined ? true : this.navParams.get('isRegister');
    }
    SpCocukPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var translationSource = __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'Child'; })[0];
        this.translation = translationSource.Translation;
        this.commonProvider.getMember().then(function (memberInfo) {
            if (memberInfo && memberInfo.MemberId && memberInfo.MemberId > 0) {
                var loading_1 = _this.loadingCtrl.create({ content: __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].loadingMsg });
                loading_1.present();
                var model = {
                    MemberId: memberInfo.MemberId,
                    Lang: __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].currentLanguage
                };
                _this.memberProvider.childList(model).subscribe(function (result) {
                    _this.isLoading = false;
                    loading_1.dismiss();
                    if (result.HasError) {
                        //this.alertCtrl.create({title : result.Error.Title, message : result.Error.Message,buttons:["OK"]}).present();
                    }
                    else {
                        _this.childList = result.Result;
                    }
                });
            }
            else {
                _this.navCtrl.setRoot("LoginPage");
            }
        });
    };
    SpCocukPage.prototype.editChild = function (childId) {
        this.navCtrl.push('QuestionFormPage', { categoryId: 4, childId: childId });
    };
    SpCocukPage.prototype.next = function () {
        this.superTabsCtrl.slideTo(6);
    };
    SpCocukPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sp-cocuk',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\sp-cocuk\sp-cocuk.html"*/'<ion-header mode="ios">\n    <ion-navbar transparent mode="ios">\n      <ion-title mode="ios">{{translation.lblHeader}} </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-list>\n    <ion-item *ngFor="let child of childList" (click)="editChild(child.Id)">\n      {{child.NameSurname}}\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n\n<ion-footer padding>\n    <button ion-button block color="pink" (click)="editChild(0)">{{translation.btnAddChild}}</button>    \n    <button *ngIf="isRegister" ion-button block (click)="next()">{{translation.btnNext}}</button>    \n</ion-footer>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\sp-cocuk\sp-cocuk.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_member__["a" /* MemberProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__["a" /* SuperTabsController */]])
    ], SpCocukPage);
    return SpCocukPage;
}());

//# sourceMappingURL=sp-cocuk.js.map

/***/ })

});
//# sourceMappingURL=16.js.map