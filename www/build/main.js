webpackJsonp([25],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MemberProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MemberProvider = /** @class */ (function () {
    function MemberProvider(apiProvider, platform) {
        this.apiProvider = apiProvider;
        this.platform = platform;
    }
    MemberProvider.prototype.profileInfo = function (model) {
        return this.apiProvider.postPromise('/Member/ProfileInfo', model);
    };
    MemberProvider.prototype.updateProfileInfo = function (model) {
        return this.apiProvider.postPromise('/Member/ProfileInfoUpdate', model);
    };
    MemberProvider.prototype.setNotification = function (memberId, notificationUserId, deviceToken) {
        var model = {
            Platform: this.platform.is('android') ? 'Android' : 'iOS',
            MemberId: memberId,
            NotificationUserId: notificationUserId,
            DeviceToken: deviceToken,
            Lang: __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */].currentLanguage
        };
        return this.apiProvider.postPromise('/Member/SaveDevice', model);
    };
    MemberProvider.prototype.childList = function (model) {
        return this.apiProvider.postPromise("/Member/ChildList", model);
    };
    MemberProvider.prototype.healthRecordList = function (model) {
        return this.apiProvider.postPromise("/Member/HealthRecordList", model);
    };
    MemberProvider.prototype.saveHealthRecord = function (model) {
        return this.apiProvider.postPromise("/Member/SaveRecord", model);
    };
    MemberProvider.prototype.notificationList = function (model) {
        return this.apiProvider.postPromise("/Member/NotificationList", model);
    };
    MemberProvider.prototype.notificationSave = function (model) {
        return this.apiProvider.postPromise("/Member/NotificationSave", model);
    };
    MemberProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__["a" /* Platform */]])
    ], MemberProvider);
    return MemberProvider;
}());

//# sourceMappingURL=member.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_common__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_member__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, actionSheetController, commonProvider, memberProvider, oneSignal, platform, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.actionSheetController = actionSheetController;
        this.commonProvider = commonProvider;
        this.memberProvider = memberProvider;
        this.oneSignal = oneSignal;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.translation = {
            lblNearBy: '',
            lblVaccinateCalendar: '',
            lblHealthProfile: '',
            lblHealthRecord: '',
            lblHealthInfo: '',
            lblEmergency: ''
        };
        this.memberId = 0;
        this.unReadMessage = 0;
        this.currentLanguage = "";
        this.currentLanguage = __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage.toString();
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var translationSource = __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'Home'; })[0];
        this.translation = translationSource.Translation;
        this.commonProvider.getMember().then(function (result) {
            if (result && result.MemberId && result.MemberId > 0) {
                _this.platform.ready().then(function () {
                    _this.setOneSignal(result.MemberId);
                });
                _this.bindNotificationList(result.MemberId);
                _this.memberId = result.MemberId;
            }
        });
    };
    HomePage.prototype.presentNotificationList = function () {
        this.navCtrl.push("NotificationListPage", { data: this.notificationList, memberId: this.memberId });
    };
    HomePage.prototype.setOneSignal = function (memberId) {
        var _this = this;
        this.oneSignal.startInit('ccd26a74-e418-44ec-8c8d-9fe8e8677006', '777932748313');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(function (jsonData) {
            var type = "";
            var notificationId = 0;
            var title = "";
            var message = "";
            if (jsonData.payload.additionalData) {
                notificationId = parseInt(jsonData.payload.additionalData.notificationId);
                type = jsonData.payload.additionalData.type;
                title = jsonData.payload.additionalData.title;
                message = jsonData.payload.body;
            }
            var buttons = [];
            if (notificationId > 0) {
                buttons = [
                    {
                        text: _this.translation.lblClose,
                        role: 'cancel',
                        handler: function () {
                            console.log();
                        }
                    }
                ];
            }
            if (title.length > 0)
                _this.showAlert(title, message, buttons);
        });
        this.oneSignal.handleNotificationOpened().subscribe(function (jsonData) {
            var type = "";
            var notificationId = 0;
            var title = "";
            var message = "";
            if (jsonData.notification.payload.additionalData) {
                notificationId = parseInt(jsonData.notification.payload.additionalData.notificationId);
                type = jsonData.notification.payload.additionalData.type;
                title = jsonData.notification.payload.additionalData.title;
                message = jsonData.notification.payload.body;
            }
            var buttons = [];
            if (notificationId > 0) {
                buttons = [
                    {
                        text: _this.translation.lblClose,
                        role: 'cancel',
                        handler: function () {
                            console.log();
                        }
                    }
                ];
            }
            if (title.length > 0)
                _this.showAlert(title, message, buttons);
        });
        this.oneSignal.getIds().then(function (result) {
            _this.memberProvider.setNotification(memberId, result.userId, result.pushToken).subscribe(function (result) {
                console.log('====================================');
                console.log("SetNotification Result");
                console.log(JSON.stringify(result));
                console.log('====================================');
            });
        });
        this.oneSignal.endInit();
    };
    HomePage.prototype.bindNotificationList = function (memberId) {
        var _this = this;
        var model = {
            MemberId: memberId,
            Lang: __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage
        };
        this.memberProvider.notificationList(model).subscribe(function (result) {
            if (result.HasError) {
            }
            else {
                _this.notificationList = result.Result;
                _this.unReadMessage = _this.notificationList.filter(function (x) { return x.IsRead == false; }).length;
            }
        });
    };
    HomePage.prototype.showAlert = function (title, message, buttons) {
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: buttons
        });
        alert.present();
    };
    HomePage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            buttons: [{
                                    text: 'Türkçe',
                                    role: 'destructive',
                                    handler: function () {
                                        _this.changeLanguage('tr');
                                    }
                                }, {
                                    text: 'العربية',
                                    handler: function () {
                                        //this.alertCtrl.create({title:'Bilgi',message:'Arapça içerikler hazırlanıyor..',buttons:["OK"]}).present();
                                        _this.changeLanguage('ar');
                                    }
                                },
                                {
                                    text: 'English',
                                    role: 'destructive',
                                    handler: function () {
                                        _this.changeLanguage('en');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.changeLanguage = function (langCode) {
        var _this = this;
        this.currentLanguage = langCode;
        var loading = this.loadingCtrl.create({ content: '' });
        loading.present();
        this.commonProvider.setLanguage(langCode).then(function () {
            __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage = langCode;
            _this.setLoadingMessage();
            _this.commonProvider.changeLanguage(langCode).then(function (result) {
                var translationSource = __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'Home'; })[0];
                _this.translation = translationSource.Translation;
                _this.platform.setDir(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage == "ar" ? "rtl" : "ltr", true);
                loading.dismiss();
            });
        });
    };
    HomePage.prototype.setLoadingMessage = function () {
        switch (__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage) {
            case 'tr':
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].loadingMsg = "İşleminiz yapılırken, lütfen bekleyiniz..";
                break;
            case 'ar':
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].loadingMsg = "يتم تنفيذ الأمر , لطفا انتظر";
                break;
            default:
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].loadingMsg = "In progress, please wait.";
                break;
        }
    };
    HomePage.prototype.openMember = function () {
        this.navCtrl.push("ProfilePage");
    };
    HomePage.prototype.emergency = function () {
        this.navCtrl.push("EmergencyPage");
    };
    HomePage.prototype.nearby = function () {
        this.navCtrl.push("NearbyPage");
    };
    HomePage.prototype.healthProfile = function () {
        this.navCtrl.push("SaglikProfilimPage");
    };
    HomePage.prototype.calendar = function () {
        this.navCtrl.push("AsiTakvimiPage");
    };
    HomePage.prototype.kayitlar = function () {
        this.navCtrl.push("SaglikKayitlarimPage");
    };
    HomePage.prototype.bilgiler = function () {
        this.navCtrl.push("SaglikBilgileriPage");
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\home\home.html"*/'<ion-header mode="ios">\n    <ion-navbar mode="ios" transparent>\n    <button ion-button class="langIco" (click)="presentActionSheet()">\n      {{currentLanguage}}\n    </button>\n    \n    <ion-title mode="ios"></ion-title>\n    <ion-buttons end>\n      <button ion-button clear class="notificationIco" (click)="presentNotificationList()">\n      <ion-icon name="ios-mail"></ion-icon> <ion-badge *ngIf="unReadMessage>0" color="primary">{{unReadMessage}}</ion-badge>\n    </button>\n      <button ion-button clear class="logged-button" (click)="openMember()">\n        <ion-icon name="md-person"> \n        </ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="logo">\n        <img src="assets/imgs/hera-logo.png">\n    </div>\n    \n    <ion-card class="menus">\n      <ion-row>\n          <ion-col class="menuButton nearby" (click)="nearby()">\n            <span><strong>{{translation.lblNearBy}}</strong></span>\n              <img src="assets/imgs/nearby.svg">\n          </ion-col>\n        </ion-row>\n      <ion-row>\n        <ion-col class="menuButton rightBorder bottomBorder" (click)="calendar()">\n          <span>{{translation.lblVaccinateCalendar.split(\' \')[0]}}<br><strong>{{ translation.lblVaccinateCalendar.replace(translation.lblVaccinateCalendar.split(\' \')[0],\'\')}}</strong></span>\n          <img src="assets/imgs/asi-takvimi.svg">\n        </ion-col>\n        <ion-col class="menuButton bottomBorder"  (click)="healthProfile()">\n          <span>{{translation.lblHealthProfile.split(\' \')[0]}}<br><strong>{{translation.lblHealthProfile.replace(translation.lblHealthProfile.split(\' \')[0],\'\')}}</strong></span>\n            <img src="assets/imgs/saglik-profilim.svg">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class="menuButton rightBorder"  (click)="kayitlar()">\n          <span>{{translation.lblHealthRecord.split(\' \')[0]}}<br><strong>{{translation.lblHealthRecord.replace(translation.lblHealthRecord.split(\' \')[0],\'\')}}</strong></span>\n            <img src="assets/imgs/saglik-kayitlarim.svg">\n        </ion-col>\n        <ion-col class="menuButton"  (click)="bilgiler()">\n          <span>{{translation.lblHealthInfo.split(\' \')[0]}}<br><strong>{{translation.lblHealthInfo.replace(translation.lblHealthInfo.split(\' \')[0],\'\')}}</strong></span>\n            <img src="assets/imgs/saglik-bilgileri.svg">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class="menuButton emergency" (click)="emergency()">\n          <span><strong>{{translation.lblEmergency}}</strong></span>\n            <img src="assets/imgs/siren.svg">\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n    <ion-row class="footer-logos">\n      <ion-col>\n        <img src="assets/imgs/gcc-logo.png" style="height: 40px;">\n      </ion-col>\n      <ion-col>\n        <img src="assets/imgs/medak-logo.png" style="height: 60px; float: right;">\n      </ion-col>\n    </ion-row>\n\n</ion-content>\n'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_common__["a" /* CommonProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_member__["a" /* MemberProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/asi-takvimi/asi-takvimi.module": [
		314,
		0
	],
	"../pages/at-modal/at-modal.module": [
		315,
		24
	],
	"../pages/emergency/emergency.module": [
		316,
		23
	],
	"../pages/login/login.module": [
		320,
		9
	],
	"../pages/nearby/nearby.module": [
		317,
		10
	],
	"../pages/notification-list/notification-list.module": [
		318,
		22
	],
	"../pages/passwort-vergessen/passwort-vergessen.module": [
		319,
		21
	],
	"../pages/profile/profile.module": [
		321,
		8
	],
	"../pages/question-form/question-form.module": [
		338,
		7
	],
	"../pages/register/register.module": [
		323,
		6
	],
	"../pages/saglik-bilgileri/saglik-bilgileri.module": [
		322,
		20
	],
	"../pages/saglik-kayitlarim/saglik-kayitlarim.module": [
		337,
		19
	],
	"../pages/saglik-profilim/saglik-profilim.module": [
		324,
		18
	],
	"../pages/sb-detail/sb-detail.module": [
		325,
		17
	],
	"../pages/sp-cocuk-ekle/sp-cocuk-ekle.module": [
		326,
		5
	],
	"../pages/sp-cocuk/sp-cocuk.module": [
		327,
		16
	],
	"../pages/sp-genel/sp-genel.module": [
		328,
		4
	],
	"../pages/sp-h-modal/sp-h-modal.module": [
		329,
		15
	],
	"../pages/sp-hamilelik/sp-hamilelik.module": [
		332,
		3
	],
	"../pages/sp-kisisel-saglik/sp-kisisel-saglik.module": [
		330,
		2
	],
	"../pages/sp-rapor/sp-rapor.module": [
		331,
		14
	],
	"../pages/sp-yakin/sp-yakin.module": [
		333,
		1
	],
	"../pages/user-agg/user-agg.module": [
		334,
		13
	],
	"../pages/user-data-agg/user-data-agg.module": [
		335,
		12
	],
	"../pages/welcome-onboard/welcome-onboard.module": [
		336,
		11
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QuestionProvider = /** @class */ (function () {
    function QuestionProvider(apiProvider) {
        this.apiProvider = apiProvider;
    }
    QuestionProvider.prototype.questionList = function (model) {
        return this.apiProvider.postPromise("/Question/QuestionList", model);
    };
    QuestionProvider.prototype.saveAnswer = function (model) {
        return this.apiProvider.postPromise("/Question/SaveAnswer", model);
    };
    QuestionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* ApiProvider */]])
    ], QuestionProvider);
    return QuestionProvider;
}());

//# sourceMappingURL=question.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AccountProvider = /** @class */ (function () {
    function AccountProvider(apiProvider) {
        this.apiProvider = apiProvider;
    }
    AccountProvider.prototype.checkInfo = function (model) {
        return this.apiProvider.postPromise('/Common/CheckInfo', model);
    };
    AccountProvider.prototype.register = function (model) {
        return this.apiProvider.postPromise('/Account/Register', model);
    };
    AccountProvider.prototype.login = function (model) {
        return this.apiProvider.postPromise('/Account/Login', model);
    };
    AccountProvider.prototype.translation = function () {
        return this.apiProvider.postPromise("/Translation/List?Lang=" + __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage, null);
    };
    AccountProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* ApiProvider */]])
    ], AccountProvider);
    return AccountProvider;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlogProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BlogProvider = /** @class */ (function () {
    function BlogProvider(apiProvider) {
        this.apiProvider = apiProvider;
    }
    BlogProvider.prototype.blogList = function () {
        var model = {
            Lang: __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage
        };
        return this.apiProvider.postPromise("/Blog/BlogList", model);
    };
    BlogProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* ApiProvider */]])
    ], BlogProvider);
    return BlogProvider;
}());

//# sourceMappingURL=blog.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(245);



Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic2_super_tabs__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_locales_tr__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_api__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_common__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_storage__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_account__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_member__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_question__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_home_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_blog__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_camera__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_launch_navigator__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_onesignal__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_globalization__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_call_number__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























Object(__WEBPACK_IMPORTED_MODULE_9__angular_common__["i" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_8__angular_common_locales_tr__["a" /* default */]);
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_7_ionic2_super_tabs__["b" /* SuperTabsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    tabsPlacement: 'top',
                    backButtonText: '',
                    iconMode: 'ios',
                    tabbarPlacement: 'bottom',
                    pageTransition: 'ios'
                }, {
                    links: [
                        { loadChildren: '../pages/asi-takvimi/asi-takvimi.module#AsiTakvimiPageModule', name: 'AsiTakvimiPage', segment: 'asi-takvimi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/at-modal/at-modal.module#AtModalPageModule', name: 'AtModalPage', segment: 'at-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/emergency/emergency.module#EmergencyPageModule', name: 'EmergencyPage', segment: 'emergency', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nearby/nearby.module#NearbyPageModule', name: 'NearbyPage', segment: 'nearby', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification-list/notification-list.module#NotificationListPageModule', name: 'NotificationListPage', segment: 'notification-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/passwort-vergessen/passwort-vergessen.module#PasswortVergessenPageModule', name: 'PasswortVergessenPage', segment: 'passwort-vergessen', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saglik-bilgileri/saglik-bilgileri.module#SaglikBilgileriPageModule', name: 'SaglikBilgileriPage', segment: 'saglik-bilgileri', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saglik-profilim/saglik-profilim.module#SaglikProfilimPageModule', name: 'SaglikProfilimPage', segment: 'saglik-profilim', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sb-detail/sb-detail.module#SbDetailPageModule', name: 'SbDetailPage', segment: 'sb-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-cocuk-ekle/sp-cocuk-ekle.module#SpCocukEklePageModule', name: 'SpCocukEklePage', segment: 'sp-cocuk-ekle', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-cocuk/sp-cocuk.module#SpCocukPageModule', name: 'SpCocukPage', segment: 'sp-cocuk', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-genel/sp-genel.module#SpGenelPageModule', name: 'SpGenelPage', segment: 'sp-genel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-h-modal/sp-h-modal.module#SpHModalPageModule', name: 'SpHModalPage', segment: 'sp-h-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-kisisel-saglik/sp-kisisel-saglik.module#SpKisiselSaglikPageModule', name: 'SpKisiselSaglikPage', segment: 'sp-kisisel-saglik', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-rapor/sp-rapor.module#SpRaporPageModule', name: 'SpRaporPage', segment: 'sp-rapor', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-hamilelik/sp-hamilelik.module#SpHamilelikPageModule', name: 'SpHamilelikPage', segment: 'sp-hamilelik', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sp-yakin/sp-yakin.module#SpYakinPageModule', name: 'SpYakinPage', segment: 'sp-yakin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-agg/user-agg.module#UserAggPageModule', name: 'UserAggPage', segment: 'user-agg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-data-agg/user-data-agg.module#UserDataAggPageModule', name: 'UserDataAggPage', segment: 'user-data-agg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome-onboard/welcome-onboard.module#WelcomeOnboardPageModule', name: 'WelcomeOnboardPage', segment: 'welcome-onboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saglik-kayitlarim/saglik-kayitlarim.module#SaglikKayitlarimPageModule', name: 'SaglikKayitlarimPage', segment: 'saglik-kayitlarim', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/question-form/question-form.module#QuestionFormPageModule', name: 'QuestionFormPage', segment: 'question-form', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["a" /* IonicStorageModule */].forRoot({ name: '__hera_db',
                    driverOrder: ['sqlite', 'websql', 'indexeddb'] }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_onesignal__["a" /* OneSignal */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                // {provide: LOCALE_ID, useValue:"tr"},
                // {provide: LOCALE_ID, useValue:"en"},
                // {provide: LOCALE_ID, useValue:"ar"},
                __WEBPACK_IMPORTED_MODULE_10__providers_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_common__["a" /* CommonProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_account__["a" /* AccountProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_member__["a" /* MemberProvider */],
                __WEBPACK_IMPORTED_MODULE_16__providers_question__["a" /* QuestionProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_blog__["a" /* BlogProvider */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_globalization__["a" /* Globalization */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_common__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_globalization__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, globalization, commonProvider) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.globalization = globalization;
        this.commonProvider = commonProvider;
        this.platform.ready().then(function () {
            _this.initializeApp();
        });
    }
    MyApp_1 = MyApp;
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.commonProvider.getLanguage().then(function (result) {
            if (result && result.length > 0) {
                MyApp_1.currentLanguage = result;
                _this.setData();
            }
            else {
                _this.globalization.getPreferredLanguage().then(function (langResult) {
                    var deviceLang = langResult.value.split('-')[0].toLowerCase();
                    if (deviceLang == 'en' || deviceLang == 'ar') {
                        MyApp_1.currentLanguage = deviceLang;
                    }
                    else {
                        MyApp_1.currentLanguage = 'tr';
                    }
                    _this.setData();
                }).catch(function (error) {
                    console.log(error);
                    MyApp_1.currentLanguage = 'tr';
                    _this.setData();
                });
            }
        });
    };
    MyApp.prototype.setLoadingMessage = function () {
        switch (MyApp_1.currentLanguage) {
            case 'tr':
                MyApp_1.loadingMsg = "İşleminiz yapılırken, lütfen bekleyiniz..";
                break;
            case 'ar':
                MyApp_1.loadingMsg = "يتم تنفيذ الأمر , لطفا انتظر";
                break;
            default:
                MyApp_1.loadingMsg = "In progress, please wait.";
                break;
        }
    };
    MyApp.prototype.setData = function () {
        var _this = this;
        this.platform.setDir(MyApp_1.currentLanguage == "ar" ? "rtl" : "ltr", true);
        this.commonProvider.translation().subscribe(function (result) {
            MyApp_1.translationSource = result.Result;
            _this.commonProvider.getMember().then(function (result) {
                if (result && result.MemberId && result.MemberId > 0) {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
                }
                else {
                    _this.rootPage = "LoginPage";
                }
                _this.setLoadingMessage();
                _this.statusBar.hide();
                _this.splashScreen.hide();
            });
        });
    };
    MyApp.loadingMsg = "İşleminiz yapılırken, lütfen bekleyiniz...";
    MyApp.currentLanguage = "en";
    MyApp.translationSource = [];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = MyApp_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\app\app.html"*/'<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_globalization__["a" /* Globalization */],
            __WEBPACK_IMPORTED_MODULE_4__providers_common__["a" /* CommonProvider */]])
    ], MyApp);
    return MyApp;
    var MyApp_1;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiProvider = /** @class */ (function () {
    function ApiProvider(http) {
        this.http = http;
        //BASE_URL : string = "http://192.168.0.9:8013/api";
        this.BASE_URL = "http://herapi.wdtajans.com/api";
    }
    ApiProvider.prototype.postPromise = function (url, data) {
        url = this.BASE_URL + url;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]();
        headers.set('Content-Type', 'application/json');
        headers.set('Accept', 'application/json');
        if (data) {
            data.Lang = __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].currentLanguage;
        }
        else {
            data = { Lang: __WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */].currentLanguage };
        }
        console.log("Api Info => " + url);
        console.log("Api Post Data => " + JSON.stringify(data));
        return this.http.post(url, data, { headers: headers });
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(167);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CommonProvider = /** @class */ (function () {
    function CommonProvider(apiProvider, storage) {
        this.apiProvider = apiProvider;
        this.storage = storage;
    }
    CommonProvider.prototype.changeLanguage = function (langCode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage = langCode;
            _this.translation().subscribe(function (result) {
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].translationSource = result.Result;
                resolve(0);
            });
        });
    };
    CommonProvider.prototype.getMember = function () {
        return this.storage.get('memberInfo');
    };
    CommonProvider.prototype.setMember = function (memberInfo) {
        return this.storage.set('memberInfo', memberInfo);
    };
    CommonProvider.prototype.setLanguage = function (langCode) {
        return this.storage.set('langCode', langCode);
    };
    CommonProvider.prototype.getLanguage = function () {
        return this.storage.get('langCode');
    };
    CommonProvider.prototype.clearMember = function () {
        return this.storage.remove('memberInfo');
    };
    CommonProvider.prototype.translation = function () {
        return this.apiProvider.postPromise("/Translation/List?Lang=" + __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */].currentLanguage, null);
    };
    CommonProvider.prototype.healthCenterList = function (model) {
        return this.apiProvider.postPromise("/Common/HealthCenterList", model);
    };
    CommonProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* ApiProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], CommonProvider);
    return CommonProvider;
}());

//# sourceMappingURL=common.js.map

/***/ })

},[225]);
//# sourceMappingURL=main.js.map