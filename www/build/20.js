webpackJsonp([20],{

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SaglikBilgileriPageModule", function() { return SaglikBilgileriPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saglik_bilgileri__ = __webpack_require__(495);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SaglikBilgileriPageModule = /** @class */ (function () {
    function SaglikBilgileriPageModule() {
    }
    SaglikBilgileriPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saglik_bilgileri__["a" /* SaglikBilgileriPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__saglik_bilgileri__["a" /* SaglikBilgileriPage */]),
            ],
        })
    ], SaglikBilgileriPageModule);
    return SaglikBilgileriPageModule;
}());

//# sourceMappingURL=saglik-bilgileri.module.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SaglikBilgileriPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_blog__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_component__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SaglikBilgileriPage = /** @class */ (function () {
    function SaglikBilgileriPage(navCtrl, loadingCtrl, blogProvider) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.blogProvider = blogProvider;
        this.translation = {};
        this.isLoading = true;
    }
    SaglikBilgileriPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var translationSource = __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */].translationSource.filter(function (x) { return x.Screen == 'Health Profile'; })[0];
        this.translation = translationSource.Translation;
        var loading = this.loadingCtrl.create({ content: __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */].loadingMsg });
        loading.present();
        this.blogProvider.blogList().subscribe(function (result) {
            loading.dismiss();
            _this.blogList = result.Result;
            _this.isLoading = false;
        });
    };
    SaglikBilgileriPage.prototype.openDetail = function (blogItem) {
        this.navCtrl.push("SbDetailPage", { blogItem: blogItem });
    };
    SaglikBilgileriPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-saglik-bilgileri',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\saglik-bilgileri\saglik-bilgileri.html"*/'<ion-header mode="ios">\n  <ion-navbar transparent mode="ios">\n    <ion-title mode="ios">{{translation.lblHeader}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-list *ngIf="!isLoading">\n    <ion-item *ngFor="let item of blogList; let orderNo = index;" (click)="openDetail(item)">\n      <span class="number">{{orderNo+1}}</span>\n      <h2>{{item.Title}}</h2>\n      <p>{{item.Abstract }}</p>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\saglik-bilgileri\saglik-bilgileri.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_blog__["a" /* BlogProvider */]])
    ], SaglikBilgileriPage);
    return SaglikBilgileriPage;
}());

//# sourceMappingURL=saglik-bilgileri.js.map

/***/ })

});
//# sourceMappingURL=20.js.map