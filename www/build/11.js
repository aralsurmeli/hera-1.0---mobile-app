webpackJsonp([11],{

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeOnboardPageModule", function() { return WelcomeOnboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__welcome_onboard__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var WelcomeOnboardPageModule = /** @class */ (function () {
    function WelcomeOnboardPageModule() {
    }
    WelcomeOnboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__welcome_onboard__["a" /* WelcomeOnboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["b" /* SuperTabsModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__welcome_onboard__["a" /* WelcomeOnboardPage */]),
            ],
        })
    ], WelcomeOnboardPageModule);
    return WelcomeOnboardPageModule;
}());

//# sourceMappingURL=welcome-onboard.module.js.map

/***/ }),

/***/ 509:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomeOnboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WelcomeOnboardPage = /** @class */ (function () {
    function WelcomeOnboardPage(navCtrl, navParams, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.superTabsCtrl = superTabsCtrl;
        this.page1 = "UserAggPage";
        this.page2 = "UserDataAggPage";
        this.page3 = "SpGenelPage";
        this.page4 = "SpYakinPage";
        this.page5 = "SpKisiselSaglikPage";
        this.page6 = "SpCocukPage";
        this.page7 = "SpHamilelikPage";
        this.page1class = "";
        this.page2class = "";
        this.page3class = "";
        this.page4class = "";
        this.page5class = "";
        this.page6class = "";
        this.page7class = "";
    }
    WelcomeOnboardPage.prototype.slideToIndex = function (index) {
        this.superTabsCtrl.slideTo(index);
    };
    WelcomeOnboardPage.prototype.onTabSelect = function (ev) {
        console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
        if (ev.index == 0) {
            this.page1class = "";
            this.page2class = "";
            this.page3class = "";
            this.page4class = "";
            this.page5class = "";
            this.page6class = "";
            this.page7class = "";
        }
        else if (ev.index == 1) {
            this.page1class = "checkmark-circle";
            this.page2class = "";
            this.page3class = "";
            this.page4class = "";
            this.page5class = "";
            this.page6class = "";
            this.page7class = "";
        }
        else if (ev.index == 2) {
            this.page1class = "checkmark-circle";
            this.page2class = "checkmark-circle";
            this.page3class = "";
            this.page4class = "";
            this.page5class = "";
            this.page6class = "";
        }
        else if (ev.index == 3) {
            this.page1class = "checkmark-circle";
            this.page2class = "checkmark-circle";
            this.page3class = "checkmark-circle";
            this.page4class = "";
            this.page5class = "";
            this.page6class = "";
            this.page7class = "";
        }
        else if (ev.index == 4) {
            this.page1class = "checkmark-circle";
            this.page2class = "checkmark-circle";
            this.page3class = "checkmark-circle";
            this.page4class = "checkmark-circle";
            this.page5class = "";
            this.page6class = "";
            this.page7class = "";
        }
        else if (ev.index == 5) {
            this.page1class = "checkmark-circle";
            this.page2class = "checkmark-circle";
            this.page3class = "checkmark-circle";
            this.page4class = "checkmark-circle";
            this.page5class = "checkmark-circle";
            this.page6class = "";
            this.page7class = "";
        }
        else if (ev.index == 6) {
            this.page1class = "checkmark-circle";
            this.page2class = "checkmark-circle";
            this.page3class = "checkmark-circle";
            this.page4class = "checkmark-circle";
            this.page5class = "checkmark-circle";
            this.page6class = "checkmark-circle";
            this.page7class = "";
        }
    };
    WelcomeOnboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-welcome-onboard',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\welcome-onboard\welcome-onboard.html"*/'<ion-content>\n\n    <super-tabs toolbarColor="light" toolbarBackground="coolprimary" indicatorColor="primary" badgeColor="light" (tabSelect)="onTabSelect($event)">\n      <super-tab [root]="page1" title="1." [icon]="page1class"></super-tab>\n      <super-tab [root]="page2" title="2." [icon]="page2class"></super-tab>\n      <super-tab [root]="page3" title="3." [icon]="page3class"></super-tab>\n      <super-tab [root]="page4" title="4." [icon]="page4class"></super-tab>\n      <super-tab [root]="page5" title="5." [icon]="page5class"></super-tab>\n      <super-tab [root]="page6" title="6." [icon]="page6class"></super-tab>\n      <super-tab [root]="page7" title="7." [icon]="page7class"></super-tab>\n    </super-tabs>\n    \n</ion-content>\n'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\welcome-onboard\welcome-onboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__["a" /* SuperTabsController */]])
    ], WelcomeOnboardPage);
    return WelcomeOnboardPage;
}());

//# sourceMappingURL=welcome-onboard.js.map

/***/ })

});
//# sourceMappingURL=11.js.map