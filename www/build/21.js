webpackJsonp([21],{

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswortVergessenPageModule", function() { return PasswortVergessenPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__passwort_vergessen__ = __webpack_require__(492);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PasswortVergessenPageModule = /** @class */ (function () {
    function PasswortVergessenPageModule() {
    }
    PasswortVergessenPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__passwort_vergessen__["a" /* PasswortVergessenPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__passwort_vergessen__["a" /* PasswortVergessenPage */]),
            ],
        })
    ], PasswortVergessenPageModule);
    return PasswortVergessenPageModule;
}());

//# sourceMappingURL=passwort-vergessen.module.js.map

/***/ }),

/***/ 492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswortVergessenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PasswortVergessenPage = /** @class */ (function () {
    function PasswortVergessenPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
        this.isenabled = false;
        this.resend = false;
        this.changepass = false;
    }
    PasswortVergessenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PasswortVergessenPage');
    };
    PasswortVergessenPage.prototype.sendCode = function () {
        this.resend = true;
    };
    PasswortVergessenPage.prototype.resendCode = function () {
        this.isenabled = true;
        this.resend = true;
    };
    PasswortVergessenPage.prototype.changePass = function () {
        this.changepass = true;
    };
    PasswortVergessenPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    PasswortVergessenPage.prototype.gotoLogin = function () {
        this.navCtrl.push("LoginPage");
    };
    PasswortVergessenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-passwort-vergessen',template:/*ion-inline-start:"D:\Projects\Mobile\hera-app\src\pages\passwort-vergessen\passwort-vergessen.html"*/'<ion-header mode="ios">\n\n  <ion-navbar mode="ios" transparent>\n    <ion-buttons left>\n            <button navPop ion-button icon-only>\n                <ion-icon name="ios-arrow-back"></ion-icon>\n            </button>\n        </ion-buttons>\n    <ion-title mode="ios">Şifremi Unuttum</ion-title>\n    <ion-buttons end> \n      \n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ng-container *ngIf="!resend">\n      <ion-row>\n          <ion-col>\n            <p>\n            E-posta adresinizi giriniz\n            </p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-8>\n              <ion-input type="number" value="" class="classic"></ion-input>\n          </ion-col>\n          <ion-col col-4>\n              <button (click)="resendCode()" ion-button color="primary">Gönder</button>\n          </ion-col>\n        </ion-row>\n  </ng-container>\n  <ng-container *ngIf="resend && !isenabled">\n      <ion-row>\n          <ion-col>\n            <p class="info">\n           \n            </p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-8>\n              <ion-input type="number" value="" class="classic"></ion-input>\n          </ion-col>\n          <ion-col col-4>\n              <button (click)="resendCode()" ion-button color="primary">Tekrar Gönder</button>\n          </ion-col>\n        </ion-row>\n  </ng-container>\n  <ng-container *ngIf="isenabled && !changepass">\n      <ion-list inset lines>\n          <ion-item>\n              <ion-label floating>Şifreniz</ion-label>\n              <ion-input [type]="passwordType" clearOnEdit="false"></ion-input>\n              <ion-icon item-end [name]="passwordIcon" class="passwordIcon" (click)=\'hideShowPassword()\'></ion-icon>\n          </ion-item>\n          <ion-item>\n              <ion-label floating>Şifrenizin Tekrarı</ion-label>\n              <ion-input [type]="passwordType" clearOnEdit="false"></ion-input>\n              <ion-icon item-end [name]="passwordIcon" class="passwordIcon" (click)=\'hideShowPassword()\'></ion-icon>\n          </ion-item>\n          <ion-item></ion-item>\n      </ion-list>\n  </ng-container>\n  <ng-container *ngIf="changepass">\n    <img src="assets/imgs/Successfully.svg" class="successfully">\n    <h2 class="h2-successfully">Şifre değişikliğiniz <br>gerçekleştirildi!</h2>\n  </ng-container>\n\n\n</ion-content>\n<ion-footer padding>\n    <button [disabled]="!isenabled" *ngIf="!changepass" (click)="changePass()" ion-button block color="primary">Şifremi Değiştir</button>\n    <button *ngIf="changepass" (click)="gotoLogin()" ion-button block color="primary">Giriş Yap</button>\n</ion-footer>'/*ion-inline-end:"D:\Projects\Mobile\hera-app\src\pages\passwort-vergessen\passwort-vergessen.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PasswortVergessenPage);
    return PasswortVergessenPage;
}());

//# sourceMappingURL=passwort-vergessen.js.map

/***/ })

});
//# sourceMappingURL=21.js.map